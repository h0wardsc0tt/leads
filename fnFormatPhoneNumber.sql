USE [db_leads]
GO
/****** Object:  UserDefinedFunction [dbo].[fnFormatPhoneNumber]    Script Date: 10/13/2016 8:40:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[fnFormatPhoneNumber](@PhoneNo VARCHAR(20))
RETURNS VARCHAR(25)
AS

BEGIN
IF CHARINDEX('x', @PhoneNo) = 0
	BEGIN
		DECLARE @numericPhone VARCHAR(25)
		SET @numericPhone = dbo.RemoveAlphaCharacters(@PhoneNo)
		IF @PhoneNo IS NULL
			BEGIN
				RETURN NULL
			END
		ELSE
			BEGIN
				DECLARE @Formatted VARCHAR(25)
				SET @Formatted = 
					CASE LEN(@numericPhone)
						WHEN 14 THEN ''+''+STUFF(STUFF(STUFF(STUFF(STUFF(@numericPhone,1,2,''),3,0,'-'),3,1,' ('),8,0,') '),13,0,'-')
						WHEN 13 THEN ''+''+STUFF(STUFF(STUFF(STUFF(STUFF(@numericPhone,1,1,''),3,0,'-'),3,1,' ('),8,0,') '),13,0,'-')
						WHEN 12 THEN ''+''+STUFF(STUFF(STUFF(STUFF(STUFF(@numericPhone,1,0,''),3,0,'-'),3,1,' ('),8,0,') '),13,0,'-')
						WHEN 11 THEN LEFT(@numericPhone,1)+STUFF(STUFF(STUFF(@numericPhone,1,1,' ('),6,0,') '),11,0,'-')
						WHEN 10 THEN STUFF(STUFF(STUFF(@numericPhone,1,0,' ('),6,0,') '),11,0,'-')
						WHEN 7 THEN STUFF(@numericPhone,4,0,'-')
					ELSE @PhoneNo
					END
			END
	END
ELSE 
	BEGIN
	SET @Formatted = @PhoneNo
	END

RETURN @Formatted
END

