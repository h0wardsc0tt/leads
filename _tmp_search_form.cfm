<!-- Search Error Message Modal -->
<div id="searchMessage" class="modal fade" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center">
            <!-- Payment Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Search Error</h4>
                </div>
                <div class="modal-body text-center">
                    <div class="alert alert-info text-center">
                        <strong><span class="glyphicon glyphicon-info-sign"></span></strong> You must enter at least one search field
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>