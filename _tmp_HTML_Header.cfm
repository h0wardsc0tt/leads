<!--- Set Defaults --->
<cfparam name="Page_MetaTitle" default="">
<cfparam name="Page_MetaKeywords" default="">
<cfparam name="Page_MetaDescription" default="">

<!--- Begin HTML --->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><cfoutput>#Page_MetaTitle#</cfoutput></title>
        <meta name="Title" content="<cfoutput>#Page_MetaTitle#</cfoutput>">
        <meta name="Description" content="<cfoutput>#Page_MetaDescription#</cfoutput>">
        <meta name="Keywords" content="<cfoutput>#Page_MetaKeywords#</cfoutput>">
        <meta name="format-detection" content="telephone=no">
        <link rel="icon" href="favicon.ico?v=4">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" href="./css/main.css?v=95">
    </head>
    <body>