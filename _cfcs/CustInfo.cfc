<cfcomponent>    
	<cffunction name="LeadsSelect" output="false" returntype="any" access="public" description="Select a lead or filtered list">
        <cfargument type="any" name="id" dbvarname="@id" required="no">
        <cfargument type="any" name="pageNumber" dbvarname="@pageNumber" required="no">
        <cfargument type="any" name="itemsPerPage" dbvarname="@itemsPerPage" required="no">
        <cfargument type="string" name="name" dbvarname="@name" required="no">
        <cfargument type="string" name="address" dbvarname="@address" required="no">
        <cfargument type="string" name="city" dbvarname="@city" required="no">
        <cfargument type="string" name="state" dbvarname="@state" required="no">
        <cfargument type="string" name="zip" dbvarname="@zip" required="no">
        <cfargument type="string" name="title" dbvarname="@title" required="no">
        <cfargument type="string" name="phone" dbvarname="@phone" required="no">
        <cfargument type="string" name="altPhone" dbvarname="@altPhone" required="no">
        <cfargument type="string" name="fax" dbvarname="@fax" required="no">
        <cfargument type="string" name="email" dbvarname="@email" required="no">
        <cfargument type="string" name="interest" dbvarname="@interest" required="no">
        <cfargument type="string" name="company" dbvarname="@company" required="no">
        <cfargument type="string" name="website" dbvarname="@website" required="no">
        <cfargument type="string" name="location" dbvarname="@location" required="no">
        <cfargument type="string" name="comment" dbvarname="@comment" required="no">
        <cfargument type="string" name="type" dbvarname="@type" required="no">
        <cfargument type="string" name="origin" dbvarname="@origin" required="no">
        <cfargument type="string" name="fromDate" dbvarname="@fromDate" required="no">
        <cfargument type="string" name="toDate" dbvarname="@toDate" required="no">
        <cfargument type="string" name="storeNumber" dbvarname="@storeNumber" required="no">
        <cfargument type="string" name="storedProcedure" dbvarname="@storedProcedure" required="no" default="dbo.[Leads_Select]">
        <cfargument type="string" name="excelSelect" dbvarname="@excelSelect" required="no">
        <cfargument type="string" name="sortColumn" dbvarname="@sortColumn" required="no">
        <cfargument type="string" name="sortOrder" dbvarname="@sortOrder" required="no">
        <cfargument type="string" name="notIn" dbvarname="@notIn" required="no">
                                
        <cfset results = "OK">
		<cftry>
 			<cfstoredproc procedure="#storedProcedure#" datasource="#APPLICATION.Datasource#" >  
            
            	<cfif isDefined("Arguments.id")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.id#>
               	<cfelse>
                	<cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
				</cfif> 

				<cfif isDefined("Arguments.pageNumber")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.pageNumber#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>

				<cfif isDefined("Arguments.itemsPerPage")>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                     	type="In"
                   		value=#Arguments.itemsPerPage#>
                 <cfelse>
                    <cfprocparam
                    	cfsqltype="cf_sql_integer"
                      	type="In"
                        null="Yes">
                 </cfif>
               
                <cfif isDefined("Arguments.name")>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                        type="In"
                        null="#NOT len(trim(Arguments.name))#"
                        value="#Arguments.name#">
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.address")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.address#"
                        null="#NOT len(trim(Arguments.address))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.city")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.city#"
                        null="#NOT len(trim(Arguments.city))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.state")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.state#"
                        null="#NOT len(trim(Arguments.state))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.zip")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.zip#"
                        null="#NOT len(trim(Arguments.zip))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.title")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.title#"
                        null="#NOT len(trim(Arguments.title))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.phone")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.phone#"
                        null="#NOT len(trim(Arguments.phone))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.altPhone")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.altPhone#"
                        null="#NOT len(trim(Arguments.altPhone))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.fax")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.fax#"
                        null="#NOT len(trim(Arguments.fax))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.email")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.email#"
                        null="#NOT len(trim(Arguments.email))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.interest")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.interest#"
                        null="#NOT len(trim(Arguments.interest))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.company")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.company#"
                        null="#NOT len(trim(Arguments.company))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.website")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.website#"
                        null="#NOT len(trim(Arguments.website))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.location")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.location#"
                        null="#NOT len(trim(Arguments.location))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.comment")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.comment#"
                        null="#NOT len(trim(Arguments.comment))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.type")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.type#"
                        null="#NOT len(trim(Arguments.type))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.origin")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.origin#"
                        null="#NOT len(trim(Arguments.origin))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.fromDate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.fromDate#"
                        null="#NOT len(trim(Arguments.fromDate))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

                <cfif isDefined("Arguments.toDate")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.toDate#"
                        null="#NOT len(trim(Arguments.toDate))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.storeNumber")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.storeNumber#"
                        null="#NOT len(trim(Arguments.storeNumber))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.excelSelect")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.excelSelect#"
                        null="#NOT len(trim(Arguments.excelSelect))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 
                    
                <cfif isDefined("Arguments.sortColumn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.sortColumn#"
                        null="#NOT len(trim(Arguments.sortColumn))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

               <cfif isDefined("Arguments.sortOrder")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.sortOrder#"
                        null="#NOT len(trim(Arguments.sortOrder))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

               <cfif isDefined("Arguments.notIn")>
                    <cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                    	type="In"
                        value="#Arguments.notIn#"
                        null="#NOT len(trim(Arguments.notIn))#"
                        >
               	<cfelse>
                	<cfprocparam
                        cfsqltype="CF_SQL_VARCHAR"
                   		type="In"
                        null="Yes">
				</cfif> 

            	<cfprocresult name="qry_getSelect">     
             </cfstoredproc>
             	            
            <cfcatch type="Database">    
                <cfset results = "#cfcatch.detail#"> 
            </cfcatch>
        </cftry> 
		<cfif isDefined("Arguments.id")>
        	<cfreturn qry_getSelect />
        <cfelse>
        	<cfreturn qry_getSelect />
        </cfif>
	</cffunction>      
    
</cfcomponent>