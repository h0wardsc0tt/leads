
<!--- Only show if User is logged in and not at Login Screen --->

    <nav class="navbar navbar">
        <div class="container">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span> 
                    </button>
                    <a class="navbar-brand navbar-logo"><img src="./images/hme-companies.png" alt="HME" height="35" /></a>
                    <a class="navbar-brand" href="#">Leads</a>
                </div>
				<div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                            <li><a href="javascript:void(0);" 
                            onclick="$('#excel_leads_excluded').val(JSON.stringify(lead_excluded));$('#excelExport').submit();">Export To Excel</a></li>
                            <li><a href="javascript:void(0);" 
                            onclick="$('#excel_save').html('');$('#excelOrder').modal('toggle');">Excel Columns</a></li>
                    </ul>
                </div>               
            </div>
        </div>
    </nav>
    



