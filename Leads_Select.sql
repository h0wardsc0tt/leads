USE [db_leads]
GO
/****** Object:  StoredProcedure [dbo].[Leads_Select]    Script Date: 10/7/2016 8:37:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[Leads_Select](
	@id INT = NULL,
	@pageNumber INT = NULL,
	@itemsPerPage INT = NULL,
	@name		NVARCHAR(50) = NULL,
	@address	NVARCHAR(255) = NULL,
	@city		NVARCHAR(50) = NULL,
	@state		NVARCHAR(50) = NULL,
	@zip		NVARCHAR(20) = NULL,
	@title		NVARCHAR(100) = NULL,
	@phone		NVARCHAR(50) = NULL,
	@altPhone	NVARCHAR(50) = NULL,
	@fax		NVARCHAR(50) = NULL,
	@email		NVARCHAR(255) = NULL,
	@interest	NVARCHAR(100) = NULL,
	@company	NVARCHAR(255) = NULL,
	@website	NVARCHAR(255) = NULL,
	@location	NVARCHAR(255) = NULL,
	@comment	NVARCHAR(MAX) = NULL,
	@type		NVARCHAR(255) = NULL,
	@origin		NVARCHAR(255) = NULL,
	@fromDate	NVARCHAR(50) = NULL,
	@toDate		NVARCHAR(50)= NULL,
	@storeNumber NVARCHAR(255) = NULL,
	@excelSelect NVARCHAR(4000) = NULL,
	@sortColumn NVARCHAR(50) = 'Lead_Created_DTS',
	@sortOrder NVARCHAR(5) = 'desc',
	@notIn NVARCHAR(4000) = NULL

) AS

DECLARE
	@select  NVARCHAR(4000),
	@from	 NVARCHAR(4000),
	@where	 NVARCHAR(4000),
	@sql	 NVARCHAR(4000),
	@orderBy NVARCHAR(4000),
	@groupBy NVARCHAR(4000),
	@rowCount   NVARCHAR(4000),
	@pageFetch  NVARCHAR(4000),
	@idNotIn  NVARCHAR(4000),
	@cnt INT,
	@count INT,
	@recordSet INT

SET @pageFetch = ''
--SET @notIn = '17,16'
IF @notIn IS NOT NULL
	BEGIN
		SET @idNotIn = ' AND lead_id NOT IN (' + @notIn + ') '	
	END
ELSE
	BEGIN
		SET @idNotIn = ''	
	END

IF @pageNumber IS NOT NULL AND  @itemsPerPage IS NOT NULL AND @excelSelect IS NULL
	BEGIN
		SET @recordSet = (@pageNumber * @itemsPerPage) - @itemsPerPage
		SET @pageFetch = ' OFFSET ' + CAST(@recordSet as NVARCHAR(5)) + ' ROWS FETCH NEXT ' +CAST(@itemsPerPage as NVARCHAR(5))  + ' ROWS ONLY'
	END


IF @id IS NOT NULL 
	BEGIN
		SELECT 
			 l.Lead_ID AS id
			,l.Lead_UID AS uid
			,l.Lead_Title AS title
			,l.Lead_FirstName + ' ' + l.Lead_LastName AS name
			,l.Lead_FirstName AS firstName
			,l.Lead_LastName AS lastName
			,l.Lead_Phone AS phone
			,l.Lead_AltPhone AS altPhone
			,l.Lead_Fax AS fax
			,l.Lead_Email AS email
			,l.Lead_City AS city
			,l.Lead_State AS state
			,l.Lead_Zip AS zip
			,l.Lead_Interest AS interest
			,l.Lead_Company AS company
			,l.Lead_Address AS address
			,l.Lead_Website AS webSite
			,l.Lead_Location AS location
			,l.Lead_Comment AS comment
			,l.Lead_Type AS type
			,l.Lead_Origin AS origin
			,l.Lead_IP AS ip
			,l.Lead_UserAgent AS userAgent
			,l.Lead_Created_DTS AS dateCreated
			,l.Lead_StoreNumber AS storeNumber
		FROM tbl_leads l 
		WHERE l.Lead_ID = @id
	END
ELSE 
	BEGIN

		IF(@sortColumn = 'Lead_Created_DTS')
			BEGIN
				SET @orderBy = ' order by ' + @sortColumn + ' ' + @sortOrder
			END
		ELSE
			BEGIN
				SET @orderBy = ' order by LOWER(ltrim(' + @sortColumn + ')) ' + @sortOrder + ', Lead_Created_DTS desc'
			END

		IF @excelSelect IS NOT NULL
			BEGIN
				SET @select = @excelSelect
			END
		ELSE
			BEGIN
				SET @rowCount = 'SELECT @cnt = count(*)'
				SET @select = 'SELECT @cnt as count,Lead_ID AS id, ''0'' AS exclude 
				,ISNULL(Lead_Title, '''') AS title
				,ISNULL(Lead_FirstName, '''') + CASE WHEN Lead_FirstName IS NULL THEN '''' ELSE '' '' END +  ISNULL(Lead_LastName, '''') AS name
				,ISNULL(Lead_FirstName, '''') AS firstName
				,ISNULL(Lead_LastName, '''') AS lastName
				,ISNULL(dbo.fnFormatPhoneNumber(Lead_Phone) , '''') AS phone
				,ISNULL(dbo.fnFormatPhoneNumber(Lead_AltPhone), '''') AS altPhone
				,ISNULL(dbo.fnFormatPhoneNumber(Lead_Fax), '''') AS fax
				,ISNULL(Lead_Email, '''') AS email
				,ISNULL(Lead_City, '''') AS city
				,ISNULL(dbo.fnstate_abbr(LTRIM(Lead_State)), '''') AS state
				,ISNULL(LTRIM(Lead_Zip), '''') AS zip
				,ISNULL(Lead_Interest, '''') AS interest
				,ISNULL(REPLACE(Lead_Company, ''"'', '''''''') , '''') AS company
				,ISNULL(Lead_Address, '''') AS address
				,ISNULL(Lead_Website, '''') AS webSite
				,ISNULL(Lead_Location, '''') AS location
				,ISNULL(REPLACE(REPLACE(Lead_Comment, ''"'', ''''''''), ''\x01'', ''''), '''') AS comment
				,ISNULL(Lead_Type, '''') AS type
				,ISNULL(Lead_Origin, '''') AS origin
				,ISNULL(Lead_IP, '''') AS ip
				,ISNULL(Lead_UserAgent, '''') AS userAgent
				,Lead_Created_DTS AS  dateCreated
				,ISNULL(Lead_StoreNumber, '''') AS storeNumber
				'
				SET @groupBy = ''--'pr.id, pr.PurchaseOrder, pr.Requestor, pr.RequestedDate, li.[key], li.ManufacturePinDescription'
		END
	  --END IF
		SET @from =  ' FROM tbl_Leads'
		SET @where = ' WHERE 1=1'

		IF @name IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_FirstName + '' '' + Lead_LastName LIKE' + '''%' + @name + '%'''
			END
		IF @address IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_Address LIKE ' + '''%' + @address + '%'''
			END
		IF @city IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_City LIKE ' + '''%' + @city + '%'''
			END
		IF @state IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_State LIKE ' + '''%' + @state + '%'''
			END

		IF @zip IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_ZIP LIKE ' + '''%' + @zip + '%'''
			END
		IF @title IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_Title LIKE ' + '''%' + @title + '%'''
			END
		IF @phone IS NOT NULL
			BEGIN
				SET @where = @where + ' AND replace(replace(replace(replace(replace(replace(Lead_phone, ''+'', ''''), '' '', ''''), '')'', ''''), ''('', ''''), ''.'', ''''), ''-'', '''') LIKE ' + '''%' + @phone + '%'''
			END
		IF @altPhone IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_altPhone LIKE ' + '''%' + @altPhone + '%'''
			END
		IF @fax IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_fax LIKE ' + '''%' + @fax + '%'''
			END
		IF @email IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_email LIKE ' + '''%' + @email + '%'''
			END
		IF @interest IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_interest LIKE ' + '''%' + @interest + '%'''
			END
		IF @company IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_company LIKE ' + '''%' + @company + '%'''
			END
		IF @website IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_website LIKE ' + '''%' + @website + '%'''
			END
		IF @location IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_location LIKE ' + '''%' + @location + '%'''
			END
		IF @comment IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_comment LIKE ' + '''%' + @comment + '%'''
			END
		IF @type IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_type LIKE ' + '''%' + @type + '%'''
			END
		IF @origin IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_origin LIKE ' + '''%' + @origin + '%'''
			END
		IF @storeNumber IS NOT NULL
			BEGIN
				SET @where = @where + ' AND Lead_storeNumber LIKE ' + '''%' + @storeNumber + '%'''
			END
		IF @fromDate IS NOT NULL AND @toDate IS NOT NULL
			BEGIN
				SET @where = @where + ' AND CAST(Lead_Created_DTS AS DATE) >=  ''' + @fromDate + '''  AND CAST(Lead_Created_DTS AS DATE) <=  ''' + @toDate + ''''
			END
		IF @fromDate IS NOT NULL AND @toDate IS NULL
			BEGIN
				SET @where = @where + ' AND CAST(Lead_Created_DTS AS DATE) >= ''' + @fromDate + ''''
			END
		IF @fromDate IS NULL AND @toDate IS NOT NULL
			BEGIN
				SET @where = @where + ' AND CAST(Lead_Created_DTS AS DATE) <=  ''' + @toDate + ''''
			END

		IF @excelSelect IS NOT NULL
			BEGIN
				SET @sql = @select + @from + @where + @idNotIn + @orderBy
				EXEC sp_executesql @sql
			END
		ELSE
			BEGIN
				SET @sql = @rowCount  + @from + @where + @idNotIn
				EXEC sp_executesql @sql, N'@cnt int OUTPUT', @cnt=@count OUTPUT
				SET @sql = @select + @from + @where + @idNotIn + @orderBy + @pageFetch
				EXEC sp_executesql @sql , N'@cnt int OUTPUT',@cnt=@count OUTPUT
			END
	END