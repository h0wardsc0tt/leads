				<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
                <script type="text/javascript" src="./js/jquery.simplePagination.js"></script>
                
                <!--- Check Page, Include necessary JS files --->
                <cfif CGI.QUERY_STRING CONTAINS "SelectLeads">
                    <script type="text/javascript" src="./js/leads.js?v=1"></script>
                </cfif>
               <!--- Include Main JS file --->
				<script>
                    $(document).ready(function() {
                        $( function() {
							$('.modal-dialog').draggable({
								handle: ".modal-header"
							});

                            $( "#excel_sortable" ).sortable({
							  	stop: function( event, ui ) {
									$('#excel_save').html('');
									$('#excel_save_btn').show();
								}
							});
                            $( "#excel_sortable" ).disableSelection();
							
							$('.excel-text').keypress(function(e) {
								$('#excel_save').html('');
								$('#excel_save_btn').show();
							});
							
							$('.excel-ck').click(function(e) {
								$('#excel_save').html('');
								$('#excel_save_btn').show();
							});
							
							$( '#excel-header').click(function(e) {
								$('#excel_save').html('');
								$('#excel_save_btn').show();
							});
							
							$( "#Lead_Date_From" ).click(function(e) {
								$(this).datepicker('setDate', null);
							});
							
							$( "#Lead_Date_To" ).click(function(e) {
								$(this).datepicker('setDate', null);
							});
							
							$( "#Lead_Date_From" ).datepicker({
								onSelect: function(){
									//if(!$("#Lead_Date_To").val().length){
									//	$("#Lead_Date_To").val($(this).val());
									//}
								}	
							});
							
							$( "#Lead_Date_To" ).datepicker({
								onSelect: function(){
									//if(!$("#Lead_Date_From").val().length){
									//	$("#Lead_Date_From").val($(this).val());
									//}
								}	
							});
							excel_order($('#excel_sortable'), '')					
							$('#lead_filter').val(leadFilter);

							//$(".sortable").addClass("sort");														

							$(".sortable" ).click(function(e) {
								sort_results($(this));
							});

							$('.header_title').removeClass('sorted');
							
							var sortedColumn = $('#' + $('#Sort_Column').val());
							var sortedColumnId = sortedColumn.prop('id');
							
							$('#' + sortedColumnId + ' span:first-child').addClass('sorted');
							sortedColumn.removeClass('sort').addClass($('#Sort_Order').val());	
											
							console.log(json);
                        });
						$(function() {
							// Paginate to Next page
							$('#CSP_Next_Page').click(function(e) {
								lead_paging(1);
								return false;
								//$('#CSP_Current_Page').val(parseInt($('#CSP_Current_Page').val())+1);
							});
							
							// Paginate to Previous page
							$('#CSP_Prev_Page').click(function(e) {
								lead_paging(-1);
								return false;
								//$('#CSP_Current_Page').val(parseInt($('#CSP_Current_Page').val())-1);
							});	
							// Toggle items per page
							$('#CSP_Per_Page').change(function(e) {
								//$('#CSP_Search_Page').val($('#Product_Search').val());
								$('#CSP_Current_Page').val(1);
								$('#Cust_Search_Form').submit();
							});
							
							// Set to Page 1 if new search
							$('#Cust_Search_Submit').click(function(e) {
								$('#CSP_Current_Page').val(1);
							});
						
							$('#Lead_Prev_Page').click(function(e) {
								if(display_lead_index == 0){
									lead_paging(-1);
								}else{
									display_lead_index -= 1;
									show_lead(display_lead_index)
								}
							});
						
							$('#Lead_Next_Page').click(function(e) {
								//console.log(display_lead_index +'   '+ (json.length - 1));
								if(display_lead_index == json.length - 1){
									lead_paging(1);
								}
								else{
									display_lead_index += 1;
									show_lead(display_lead_index)
								}
							});						
						});						
	                });					
                </script>
            </div>
        </div>
	</body>
</html>


