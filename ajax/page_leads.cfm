<cfsetting showdebugoutput="no">
<cfscript>		
	if (#FindNoCase('application/json', cgi.content_type)# > 0)
	{
    	leadFilter = deserializeJSON(ToString(getHTTPRequestData().content));	
		leadFilter['storedProcedure'] = 'dbo.[Leads_Select]';
		object = CreateObject("component", "_cfcs.CustInfo");
		returnSelect = object.LeadsSelect(argumentCollection = leadFilter);
		returnResult = StructNew();
		returnResult['itemsperpage'] = leadFilter['itemsperpage'];
		returnResult['pagenumber'] = leadFilter['pagenumber'];
		if(returnSelect.RecordCount > 0)
			returnResult['leadcount'] = returnSelect['count'];
		else
			returnResult['leadcount'] = '0';
		deleteStatus = StructDelete(leadFilter,'storedProcedure','True');
		returnResult['returnSelect'] = returnSelect;	
		returnResult['leadFilter'] = StructCopy(leadFilter);	
		returnResult['page_start'] = ((returnResult['pagenumber'] - 1) * returnResult['itemsperpage']) + 1;	
		returnResult['page_end'] = returnResult['pagenumber'] * returnResult['itemsperpage'];	
 		StructClear(leadFilter);		
	}
	else
	{
		returnResult = StructNew();
	}
	
	serializer = new lib.JsonSerializer();
	writeOutput(serializer.serialize(returnResult));
	StructClear(leadFilter);		
</cfscript>
