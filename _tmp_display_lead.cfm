
<div id="display_lead_modal" class="modal fade "data-backdrop="static" role="dialog">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width:100%;">
            <!-- Payment Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Lead</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                       <div class="col-xs-12" >
                            <div class="col-xs-12"><label for="excel_exclude" style="padding-right:5px;">Exclude from Excel:</label>
                            	<input id="excel_exclude" type="checkbox" />
                            </div>
						</div>
                        <div class="col-xs-12">
                            <div class="col-xs-4"><label for="display_firstname">First Name:</label>
                            	<span id="display_firstname"></span>
                            </div>
                            <div class="col-xs-4"><label for="display_lastname">Last Name:</label>
                            	<span id="display_lastname"></span>
                            </div>    
                           <div class="col-xs-4"><label for="display_email" style="padding-right:5px;">eMail:</label>
                            	<span id="display_email"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-4"><label for="display_address">Address:</label>
                            	<span id="display_address"></span>
                            </div>
                            <div class="col-xs-4"><label for="display_city">City:</label>
                            	<span id="display_city"></span>
                            </div>    
                           <div class="col-xs-4"><label for="display_state" style="padding-right:5px;">State:</label>
                            	<span id="display_state"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-4"><label for="display_zip">Zip:</label>
                            	<span id="display_zip"></span>
                            </div>
                            <div class="col-xs-4"><label for="display_phone">Phone:</label>
                            	<span id="display_phone"></span>
                            </div>    
                           <div class="col-xs-4"><label for="display_altphone" style="padding-right:5px;">Altphone:</label>
                            	<span id="display_altphone"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-4"><label for="display_company">Company:</label>
                            	<span id="display_company"></span>
                            </div>
                            <div class="col-xs-4"><label for="display_storenumber">Store Number:</label>
                            	<span id="display_storenumber"></span>
                            </div>    
                           <div class="col-xs-4"><label for="display_title" style="padding-right:5px;">Title:</label>
                            	<span id="display_title"></span>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-4"><label for="display_type">Type:</label>
                            	<span id="display_type"></span>
                            </div>
                            <div class="col-xs-4"><label for="display_interest">Interest:</label>
                            	<span id="display_interest"></span>
                            </div>    
                           <div class="col-xs-4"><label for="display_location" style="padding-right:5px;">Location:</label>
                            	<span id="display_location"></span>
                            </div>
                        </div>
                       <div class="col-xs-12">
                            <div class="col-xs-4"><label for="display_fax">Fax:</label>
                            	<span id="display_fax"></span>
                            </div>
                            <div class="col-xs-4"><label for="display_ip">Ip:</label>
                            	<span id="display_ip"></span>
                            </div>    
                           <div class="col-xs-4"><label for="display_origin" style="padding-right:5px;">Origin:</label>
                            	<span id="display_origin"></span>
                            </div>
                        </div>
                       <div class="col-xs-12">
                            <div class="col-xs-4"><label for="display_website">Website:</label>
                            	<span id="display_website"></span>
                            </div>
                            <div class="col-xs-4"><label for="display_datecreated">Date Entered:</label>
                            	<span id="display_datecreated"></span>
                            </div>    
                        </div>
                        <div class="col-xs-12" >
                            <div class="col-xs-12"><label for="display_comment" style="padding-right:5px;">Comments:</label>
                            	<div id="display_comment" class="col-xs-12">
                            	</div>
                            </div>
                        </div>
                        <div class="col-xs-12" >
                            <div class="col-xs-12"><label for="display_useragent" style="padding-right:5px;">User Agent:</label>
                            	<div id="display_useragent" class="col-xs-12"></div>
                            </div>
						</div>
					</div>
                </div>
                <div class="modal-footer">
                	<div class="col-lg-1 no-pad">
                		<button id="Lead_Prev_Page" name="Lead_Prev_Page" class="btn btn-primary btn-left display-lead-button">
                        	<span class="glyphicon glyphicon-arrow-left"></span>
                        </button>
                        <button id="Lead_Next_Page" name="Lead_Next_Page" class="btn btn-primary btn-right pull-right display-lead-button">
                        	<span class="glyphicon glyphicon-arrow-right"></span>
                        </button>

                    </div> 
                    <div class="col-lg-2 no-pad">
                        <div class="col-lg-11 text-left" style="display:inline-block; font-size:10px;padding-top: 10px;">
                        <cfoutput>
                        	<div class="csp-results">Showing 
                            <span class="csp-page-start">#VARIABLES.Page_Start#</span> - <span class="csp-page-end">#VARIABLES.Page_End#</span>
                              of (<span class="total_leads">#CustomerCount#</span>)  Results
                        	</div>
                         </cfoutput>
                 		</div>
                    </div>                   
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>