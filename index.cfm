<cfsetting showdebugoutput="no">
<cfif (CGI.QUERY_STRING DOES NOT CONTAIN "pg=SelectLeads" AND CGI.QUERY_STRING DOES NOT CONTAIN "pg=ShowLead")>
	<cflocation url="./?pg=SelectLeads" addtoken="no">
    <cfabort>
</cfif>
<cfinclude template="./_tmp_serialize.cfm">
<cfinclude template="./_tmp_HTML_Header.cfm">
<cfinclude template="./_tmp_HTML_Navigation_LoggedIn.cfm">

<cfinclude template="./_dat_customer_prep.cfm">
<cfinclude template="./_tmp_customer_lookup.cfm">
<cfinclude template="./_tmp_excel_column_order.cfm">
<cfinclude template="./_tmp_display_lead.cfm">
<cfinclude template="./_tmp_HTML_Footer.cfm">
