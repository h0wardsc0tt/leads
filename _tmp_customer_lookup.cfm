<!--- Set Defaults --->
<cfparam name="FORM.Cust_Number_Anchor" default="">
<cfoutput>
    <div class="col-lg-12">
    	<h2>Select Leads</h2>
    </div>    
	<!--- Customer Search Form --->
    <form action="" method="POST" id="Cust_Search_Form" class="form-horizontal col-lg-12" role="form" onsubmit="return form_submit()">
    	<div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="Lead_Name" class="control-label col-lg-5">Lead Name:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_Name" id="Lead_Name" class="form-control" value="#FORM.Lead_Name#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_City" class="control-label col-lg-5">City:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_City" id="Lead_City" class="form-control" value="#FORM.Lead_City#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_State" class="control-label col-lg-5">State:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_State" id="Lead_State" class="form-control" value="#FORM.Lead_State#" />
                    </div>
                </div>
                <div class="form-group">
                        <label for="Lead_Type" class="control-label col-lg-5">Type:</label>
                        <div class="col-lg-7">
                            <input type="text" maxlength="100" name="Lead_Type" id="Lead_Type" class="form-control" value="#FORM.Lead_Type#" />
                        </div>
                    </div>
                </div>
            <div class="col-lg-4">
                <div class="form-group">
                    <label for="Lead_Zip" class="control-label col-lg-5">Postal Code:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_Zip" id="Lead_Zip" class="form-control" value="#FORM.Lead_Zip#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_Phone" class="control-label col-lg-5">Phone:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_Phone" id="Lead_Phone" class="form-control" value="#FORM.Lead_Phone#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_Email" class="control-label col-lg-5">Email:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_Email" id="Lead_Email" class="form-control" value="#FORM.Lead_Email#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_Date_From" class="control-label col-lg-5">Date From: <div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_Date_From" id="Lead_Date_From" class="form-control cal-field" value="#FORM.Lead_Date_From#" readonly />
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
            	<div class="form-group">
                    <label for="Lead_Company" class="control-label col-lg-5">Company:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_Company" id="Lead_Company" class="form-control" value="#FORM.Lead_Company#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_StoreNumber" class="control-label col-lg-5">Store Number:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_StoreNumber" id="Lead_StoreNumber" class="form-control" value="#FORM.Lead_StoreNumber#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_Interest" class="control-label col-lg-5">Interest:</label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_Interest" id="Lead_Interest" class="form-control" value="#FORM.Lead_Interest#" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="Lead_Date_To" class="control-label col-lg-5">Date To: <div class="cal-cont"><span class="glyphicon glyphicon-calendar"></span></div></label>
                    <div class="col-lg-7">
                        <input type="text" maxlength="100" name="Lead_Date_To" id="Lead_Date_To" class="form-control cal-field" value="#FORM.Lead_Date_To#" readonly />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4"></div>
            <div class="col-lg-4">
                <div class="form-group">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6 no-pad-search">
                        <input type="submit" name="Cust_Search_Submit" id="Cust_Search_Submit" class="btn btn-primary form-control" value="Search" />
 						<input type="hidden" name="Sort_Column" id="Sort_Column"  value="#VARIABLES.Sort_Column#"/>
						<input type="hidden" name="Sort_Order" id="Sort_Order"  value="#VARIABLES.Sort_Order#"/>
                   </div>
                </div>
            </div>
        </div>
    
        <div class="col-lg-12">&nbsp;</div>
        
        <!--- Customer Search Results --->
        <div class="col-lg-12 table-responsive no-pad">
            <h4>Search Results</h4>
            <table id="Cust_Results" class="table " style="white-space:nowrap;margin:0px;">
                <thead id="Cust_Results_Header">
                    <tr>
						<th style="width:3%;">Exc.</th>                    
                        <th style="width:15%;"><a id="firstname" href="javascript:void(0);" class="sortable sort"><span class="header_title">NAME</span></a></th>
                        <th style="width:19%;"><a id="company" href="javascript:void(0);" class="sortable sort"><span class="header_title">COMPANY</span></a></th>
                        <th style="width:19%;"><a id="type" href="javascript:void(0);" class="sortable sort"><span class="header_title">TYPE</span></a></th>
                        <th style="width:10%;"><a id="city" href="javascript:void(0);" class="sortable sort"><span class="header_title">CITY</span></a></th>
                        <th style="width:10%;"><a id="state" href="javascript:void(0);" class="sortable sort"><span class="header_title">STATE</span></a></th>
                        <th style="width:10%;"><a id="zip" href="javascript:void(0);" class="sortable sort"><span class="header_title">POSTCODE</span></a></th>
                        <th style="width:10%;"><a id="phone" href="javascript:void(0);" class="sortable sort"><span class="header_title">PHONE</span></a></th>
                        <th style="width:4%;"><a id="Created_DTS" href="javascript:void(0);" class="sortable sort"><span class="header_title">DATE</span></a></th>
                    </tr>
                </thead>
                <tbody>

                    <cfloop from="1" to="#ArrayLen(returnSelectResult.DATA.id)#" index="cust">
                    	<cfset CustomerCount = #returnSelectResult.DATA.count[cust]#>
                        <tr class="exclude" id="tr#returnSelectResult.DATA.id[cust]#">
                        	<td><input type="checkbox" id="ck#returnSelectResult.DATA.id[cust]#" onclick="ckbox_clicked(this,'#returnSelectResult.DATA.id[cust]#')" class="excel-exclude-ckbox"></td>
                            <td><a href="javascript:void(0);" onclick="displayLead('#returnSelectResult.DATA.id[cust]#');">#returnSelectResult.DATA.name[cust]#</a></td>
                            <td>#returnSelectResult.DATA.company[cust]#</td>
                            <td>#returnSelectResult.DATA.type[cust]#</td>
                            <td>#returnSelectResult.DATA.city[cust]#</td>
                            <td>#returnSelectResult.DATA.state[cust]#</td>
                            <td>#returnSelectResult.DATA.zip[cust]#</td>
                            <td>#returnSelectResult.DATA.phone[cust]#</td>
                            <td>#DateFormat(returnSelectResult.DATA.dateCreated[cust],"mm/dd/yyyy")#</td>
                        </tr>
                    </cfloop>
                </tbody>
            </table>
           
            <div class="col-lg-12 no-pad csp-paginate-cont">
                <div class="col-lg-6 no-pad">
                    <div class="col-lg-1 no-pad">
                        <button id="CSP_Prev_Page" name="CSP_Prev_Page" class="btn btn-primary btn-left" <cfif VARIABLES.Page_Start EQ 1>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-left"></span></button>
                    </div>
                    <div class="col-lg-11 text-left">
                        <div class="csp-perpage">
                            <span>
                                Show
                                <select id="CSP_Per_Page" name="CSP_Per_Page">
                                    <option value="5" <cfif FORM.CSP_Per_Page EQ 5>selected="selected"</cfif>>5</option>
                                    <option value="10" <cfif FORM.CSP_Per_Page EQ 10>selected="selected"</cfif>>10</option>
                                    <option value="20" <cfif FORM.CSP_Per_Page EQ 20>selected="selected"</cfif>>20</option>
                                </select>
                                items per page
                           </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 no-pad">
                    <div class="col-lg-11 text-right"><div class="csp-results">Showing <span class="csp-page-start">#VARIABLES.Page_Start#</span> - <span class="csp-page-end">#VARIABLES.Page_End#</span> of (<span class="total_leads">#CustomerCount#</span>) Results</div></div>
                    <div class="col-lg-1 no-pad">
                        <button id="CSP_Next_Page" name="CSP_Next_Page" class="btn btn-primary btn-right pull-right" <cfif VARIABLES.Page_End GTE CustomerCount>disabled="disabled"</cfif>><span class="glyphicon glyphicon-arrow-right"></span></button>
                    </div>
                </div>
                <input type="hidden" id="CSP_Current_Page" name="CSP_Current_Page" value="#FORM.CSP_Current_Page#" />
            </div>
        </div>
    </form>
    
    <form name="Cust_Anchor_Form" id="Cust_Anchor_Form" method="post" action="./?pg=ShowLead">
		<input type="hidden" name="Cust_Number_Anchor" id="Cust_Number_Anchor" value="#FORM.Cust_Number_Anchor#" />
    </form>
    <form name="excelExport" id="excelExport" method="post" action="./excel.cfm" target="excel_target">
		<input type="hidden" name="lead_filter" id="lead_filter" />
		<input type="hidden" name="excel_columns" id="excel_columns"/>		
		<input type="hidden" name="excel_leads_excluded" id="excel_leads_excluded"/>		
		<input type="hidden" name="show_excel_header" id="show_excel_header" value="yes"/>		
    </form> 
    <iframe id="excel_target" name="excel_target" style="display:none;"></iframe> 
<script type="text/javascript">
var json = JSON.parse('#leads_json#');
var leadFilter = '#leadFilter_json#';
var lead_count = #CustomerCount#;
</script>

</cfoutput>

<cfinclude template="./_tmp_search_form.cfm">

