USE [db_leads]
GO
/****** Object:  UserDefinedFunction [dbo].[fnstate_abbr]    Script Date: 10/7/2016 11:21:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[fnstate_abbr](@parm VARCHAR(50))
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE @alphaparm nvarchar(50)
	DECLARE @state nvarchar(2)

	SET @alphaparm = upper(dbo.fn_StripCharacters(@parm, '^a-z ''^-'))

	IF LEN(@alphaparm) = 0
	BEGIN
		RETURN @parm
	END

	IF LEN(@alphaparm) = 2
	BEGIN
		RETURN @alphaparm
	END

	SET @state = ''
	SELECT @state = CASE WHEN LEN(abbreviation) > 0  THEN abbreviation END
	FROM state WHERE name = @alphaparm

	IF @state = ''
		BEGIN
			RETURN @parm
		END

	RETURN @state
END
