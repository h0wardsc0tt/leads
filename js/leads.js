var excel_json = [];
var lead_excluded = [];
var display_lead_index
function displayLead(cust){
	display_lead_index = json.map(function (id) { return id['id']; }).indexOf(cust);
	show_lead(display_lead_index)
}

function show_lead(index){
	var lead_index = lead_excluded.map(function (id) { return id['id']; }).indexOf(json[index].id);
	$.each(json[index], function(key, value) {
		if(key != 'sortcolumn' && key != 'sortorder')
			$('#display_' + key).html(value);
    });

	$('#excel_exclude').attr('onclick','lead_ckbox_clicked(this,"' + json[index].id + '")');
	$('#excel_exclude').prop('checked', (lead_index >= 0 ? true : false));	
	$('.display-lead-button').prop('disabled', false);
	if(index == 0 && $('#CSP_Current_Page').val() == '1')
		$('#Lead_Prev_Page').prop('disabled', true);
		
	if((index + 1) == json.length && ($('#CSP_Per_Page').val() * $('#CSP_Current_Page').val()) >= lead_count)
		$('#Lead_Next_Page').prop('disabled', true);

	$('#display_lead_modal').modal('show');	
}

function excel_order(obj, msg){
	var excel_json = [];
	obj.each(function(i, items_list){
        $(items_list).find('li').each(function(j, li){
			var excel_item = {}, column = $(li);
			if(column.children('input[type=checkbox]').prop('checked')){
				excel_item.column = column.children('div:first').html();
				excel_item.name = column.children('input[type=text]').val() == '' ? column.children('div:first').html() : column.children('input[type=text]').val();
				excel_json.push(excel_item);
			}
        });
    });
	$('#excel_columns').val(JSON.stringify(excel_json));
	$('#excel_save').html(msg);
	$('#excel_save_btn').hide();
	$('#show_excel_header').val($('input[name=excel-header]').val());
	console.log(excel_json);
}

function sort_results(obj){	
	if(obj.hasClass('sort') || obj.hasClass('desc')){
		$(".sortable").removeClass('sort').removeClass('desc').removeClass('asc');																												
		$(".sortable").addClass('sort');														
		obj.removeClass('sort').addClass('asc');
		$('#Sort_Order').val('asc');
	}else{
		$('.sortable').removeClass('sort').removeClass('desc').removeClass('asc');																												
		$('.sortable').addClass('sort');														
		obj.removeClass('sort').addClass('desc');
		$('#Sort_Order').val('desc');
	}
	$('#Sort_Column').val(obj.prop('id'));
	$('#CSP_Current_Page').val('1');
	var json_update = {};
	var leadFilter_update = JSON.parse(leadFilter);
	leadFilter_update.itemsperpage = $('#CSP_Per_Page').val();
	leadFilter_update.pagenumber = $('#CSP_Current_Page').val();
	leadFilter_update.sortorder = $('#Sort_Order').val();
	leadFilter_update.sortcolumn = 'Lead_' + $('#Sort_Column').val();
	json_update.leadFilter = leadFilter_update;
	page_leads_ajax(JSON.stringify(leadFilter_update));
}

function page_leads_ajax(data){
	$.ajax({
		type: "POST",
		contentType: "application/json; charset=utf-8",
		url: "ajax/page_leads.cfm",
		data: data,
		dataType: "json",
		success: function (data) {					
			if (data !== null) {
				//console.log(data);
				display_selected_leads(data);
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			console.log(thrownError);
        }
	});
}

function display_selected_leads(leads){
	var tbody = '';
	var lead_index;
	$.each(leads.returnselect, function(index, lead) {
		lead_index = lead_excluded.map(function (id) { return id['id']; }).indexOf(lead.id);
		tbody += '<tr class="exclude' + (lead_index >= 0 ? ' excel-excluded' : '') + '" ><td><input type="checkbox" id="ck' + lead.id + '" onclick="ckbox_clicked(this,\'' + lead.id + '\');" class="excel-exclude-ckbox"' + (lead_index >= 0 ? ' checked=checked' : '') + '></td>';
		tbody += '<td><a href="javascript:void(0);" onclick="displayLead(\'' + lead.id + '\');">' + lead.firstname + ' ' + lead.lastname + '</a></td>';
		tbody += '<td>' + lead.company  + '</td>';
		tbody += '<td>' + lead.type  + '</td>';
		tbody += '<td>' + lead.city  + '</td>';
		tbody += '<td>' + lead.state  + '</td>';
		tbody += '<td>' + lead.zip  + '</td>';
		tbody += '<td>' + lead.phone  + '</td>';
		tbody += '<td>' + $.date(new Date(lead.datecreated))  + '</td></tr>';
    });
	$('#Cust_Results tbody').empty();
	$('#Cust_Results tbody').html(tbody);
	json = leads.returnselect;
	leadFilter = JSON.stringify(leads.leadfilter);
	$('#CSP_Current_Page').val(leads.pagenumber);
	$('.csp-page-start').html(leads.page_start);
	$('.csp-page-end').html(leads.page_end);
	$('.total_leads').html(leads.leadcount);
	lead_count = leads.leadcount;
	$('#CSP_Prev_Page').prop('disabled',false);
	$('#CSP_Next_Page').prop('disabled',false);
	if( $('#CSP_Current_Page').val() == '1')
		$('#CSP_Prev_Page').prop('disabled',true);
	else
		if(($('#CSP_Per_Page').val() * $('#CSP_Current_Page').val()) >= lead_count)
			$('#CSP_Next_Page').prop('disabled',true);
	
	if(lead_display_direction != 0 && $('#display_lead_modal').hasClass('in')){
		if(lead_display_direction < 0){
			display_lead_index = json.length - 1;
			show_lead(display_lead_index);
			lead_display_direction = 0;
		}
		else{
			display_lead_index = 0;
			show_lead(display_lead_index);
			lead_display_direction = 0;
		}
	}
	
}

$.date = function(dateObject) {
    var d = new Date(dateObject);
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = month + "/" + day + "/" + year;

    return date;
};

var lead_display_direction = 0;
function lead_paging(direction){
	lead_display_direction = direction;
	var json_update = {};
	var leadFilter_update = JSON.parse(leadFilter);
	leadFilter_update.itemsperpage = $('#CSP_Per_Page').val();
	leadFilter_update.pagenumber = (parseInt($('#CSP_Current_Page').val()) + direction).toString();
	json_update.leadFilter = leadFilter_update;
	page_leads_ajax(JSON.stringify(leadFilter_update));
}

function ckbox_clicked(obj, id){
	var jsonObj = {};
	var index = json.map(function (id) { return id['id']; }).indexOf(id);
	var lead_index = lead_excluded.map(function (id) { return id['id']; }).indexOf(id);
	if(obj.checked){
		$(obj).closest('tr').addClass('excel-excluded')
		json[index].exclude = '1';
		jsonObj.id = id;
		lead_excluded.push(jsonObj);
	}else{
		$(obj).closest('tr').removeClass('excel-excluded')
		json[index].exclude = '0';
		lead_excluded.splice(lead_index,1);
	}
}

function lead_ckbox_clicked(obj,id){
	var jsonObj = {};
	var index = json.map(function (id) { return id['id']; }).indexOf(id);
	var lead_index = lead_excluded.map(function (id) { return id['id']; }).indexOf(id);
	if(obj.checked){
		$('#tr' + id).addClass('excel-excluded');
		$('#ck' + id).prop('checked', true);
		json[index].exclude = '1';
		jsonObj.id = id;
		lead_excluded.push(jsonObj);
	}else{
		$('#tr' + id).removeClass('excel-excluded')
		$('#ck' + id).prop('checked', false);
		json[index].exclude = '0';
		lead_excluded.splice(lead_index,1);
	}
}

function form_submit(){
	var leadFilter_update = JSON.parse(leadFilter);
	leadFilter_update.itemsperpage = $('#CSP_Per_Page').val();
	leadFilter_update.pagenumber = '1';
	leadFilter_update.name = $('#Lead_Name').val();
	leadFilter_update.city = $('#Lead_City').val();
	leadFilter_update.state = $('#Lead_State').val();
	leadFilter_update.zip = $('#Lead_Zip').val();
	leadFilter_update.email = $('#Lead_Email').val();
	leadFilter_update.company = $('#Lead_Company').val();
	leadFilter_update.storeNumber = $('#Lead_StoreNumber').val();
	leadFilter_update.company = $('#Lead_Company').val();
	leadFilter_update.phone = $('#Lead_Phone').val();
	leadFilter_update.type = $('#Lead_Type').val();
	leadFilter_update.fromDate = $('#Lead_Date_From').val();
	leadFilter_update.toDate = $('#Lead_Date_To').val();
	leadFilter = JSON.stringify(leadFilter_update);
	$('#lead_filter').val(leadFilter);
	page_leads_ajax(leadFilter);
	return false;
}